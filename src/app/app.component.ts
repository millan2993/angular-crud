import { Component, OnInit } from '@angular/core';
import { Employee } from './interfaces/employee';
import { EmployeeService } from './Services/employee.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
  title = 'crud';

  employees: Employee[] = [];

  creating = false;

  editing = false;

  constructor(private employeeService: EmployeeService) { }

  ngOnInit(): void {
    this.employeeService.getAll()
      .subscribe((employees: Employee[]) => this.employees = employees);
  }

  toCreate() {
    if (this.editing) {
      this.editing = false;
    }

    this.creating = !this.creating;
  }


  toEdit(employee: Employee) {
    this.employeeService.setEmployee(employee);

    if (this.creating) {
      this.creating = false;
    }

    this.editing = !this.editing;
  }

  create(employee: Employee) {
    this.employeeService.create(employee)
      .subscribe(res => {
        this.creating = !this.creating;

        // @ts-ignore
        const emp: Employee = res.employee;
        this.employees.push(emp);

        // @ts-ignore
        console.log(res.message);

      }, error1 => {
        console.log(error1);
      });
  }


  edit(employee: Employee) {
    this.editing = false;

    this.employeeService.update(employee).subscribe(res => {
      // @ts-ignore
      console.log(res.message);

      const index = this.employees.findIndex(em => em.id === employee.id);
      this.employees[index] = employee;
    });
  }


  remove(id: number) {
    this.employeeService.delete(id).subscribe(res => {
      const index = this.employees.findIndex(em => em.id === id);
      this.employees.splice(index, 1);

      // @ts-ignore
      console.log(res.message);
    });
  }


}
