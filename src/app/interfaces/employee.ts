export interface Employee {
  id: number;
  first_name: string;
  last_name: string;
  phone: number;
  email: string;
  address: string;
}
