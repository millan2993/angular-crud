import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Employee } from '../../interfaces/employee';

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.sass']
})
export class CreateEmployeeComponent implements OnInit {

  form: FormGroup;

  @Output()
  save = new EventEmitter<Employee>();

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      first_name: [ '', Validators.required ],
      last_name: [ '', Validators.required ],
      phone: [ '' ],
      email: [ '' ],
      address: [ '' ]
    });
  }

  ngOnInit() {
  }

  submit() {
    if (this.form.invalid) {
      console.log('Campos incorrectos');
      // return;
    }

    const employee: Employee = this.form.value;
    this.save.emit(employee);
  }


}
