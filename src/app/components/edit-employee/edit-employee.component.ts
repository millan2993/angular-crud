import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Employee } from '../../interfaces/employee';
import { EmployeeService } from '../../Services/employee.service';

@Component({
  selector: 'app-edit-employee',
  templateUrl: './edit-employee.component.html',
  styleUrls: ['./edit-employee.component.sass']
})
export class EditEmployeeComponent implements OnInit, OnDestroy {

  formEdit: FormGroup;

  employee: Employee = null;

  @Output()
  update = new EventEmitter<Employee>();

  constructor(private fb: FormBuilder,
              private employeeService: EmployeeService) {

    this.employeeService.currentEmployee().subscribe((employee: Employee) => {
      this.employee = employee;

      this.formEdit = this.fb.group({
        first_name: [ employee.first_name, Validators.required ],
        last_name: [ employee.last_name, Validators.required ],
        phone: [ employee.phone ],
        email: [ employee.email ],
        address: [ employee.address ]
      });
    });

  }


  ngOnInit() {
  }


  async submit() {
    const employee: Employee = await this.formEdit.value;

    const temp: Employee = this.employee;
    temp.first_name = employee.first_name;
    temp.last_name = employee.last_name;
    temp.phone = employee.phone;
    temp.email = employee.email;
    temp.address = employee.address;

    await this.update.emit(temp);
  }


  ngOnDestroy(): void {
    // this.employeeService.setEmployee(null);
  }
}
