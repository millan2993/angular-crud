import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';
import { Employee } from '../interfaces/employee';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  private urlBase: string;

  private current: BehaviorSubject<Employee> = new BehaviorSubject<Employee>(null);

  currentEmployee() {
    return this.current.asObservable();
  }

  setEmployee(employee: Employee) {
    this.current.next(employee);
  }

  constructor(private http: HttpClient) {
    this.urlBase = environment.url_base + 'employees';
  }

  getAll(): Observable<Employee[]> {
    return this.http.get<Employee[]>(this.urlBase);
  }

  create(employee: Employee) {
    return this.http.post(this.urlBase, employee);
  }

  update(employee: Employee) {
    const url = this.urlBase + '/' + employee.id;
    return this.http.put(url, employee);
  }

  delete(id: number) {
    const url = this.urlBase + '/' + id;
    return this.http.delete(url);
  }

}
